from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('mainEarnable/', include('mainEarnable.urls')),
    path('admin/', admin.site.urls),
]