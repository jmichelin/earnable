from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:family_id>/', views.get_family, name='detail'),
    path('<int:family_id>/members/', views.get_family_members, name='members'),
    path('<int:family_id>/add_member/', views.add_member, name='add'),
]
