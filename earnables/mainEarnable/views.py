from django.shortcuts import get_object_or_404, render
from django.template import loader
from django.http import HttpResponse

# Import models here
from .models import Family

# Create your views here.
def index(request):
    latest_family_list = Family.objects.order_by('full_name')[:5]
    context = {
        'latest_family_list':latest_family_list,
    }
    return render(request, 'main/index.html', context)

def get_family(request, family_id):
    context =  {
        'family_id' : family_id,
    }
    return render(request, 'main/get-family.html', context)

def get_family_members(request, family_id):
    family = get_object_or_404(Family, pk=family_id)
    return render(request, 'main/get_family_members.html', {'family': family})

def add_member(request, family_id):
    return HttpResponse("You're adding a member to Family %s." % family_id)
