from django.contrib import admin
from .models import Family, FamilyMember, Earnable, ActionHistory

# Register your models here.
admin.site.register(Family)
admin.site.register(FamilyMember)
admin.site.register(Earnable)
admin.site.register(ActionHistory)