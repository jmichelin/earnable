from django.db import models
from django.utils.translation import gettext as _

# Create your models here.
class Family(models.Model):
    full_name = models.CharField(max_length=200)
    nickname = models.CharField(max_length=200)
    def __str__(self):
        return self.nickname

class FamilyMember(models.Model):
    family = models.ForeignKey(Family, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=200)
    nickname = models.CharField(max_length=200)
    password = models.CharField(max_length=20)
    avatar = models.ImageField(upload_to = 'uploads/avatars')
    isAdmin = models.BooleanField()
    member_age = models.PositiveSmallIntegerField
    def __str__(self):
        return self.nickname

class Earnable(models.Model):
    family_member = models.ForeignKey(FamilyMember, on_delete=models.CASCADE)
    task = models.CharField(max_length=200)
    time_earnable = models.IntegerField(default=0)
    earned_action = models.CharField(max_length=200)
    def __str__(self):
        return self.task    

class ActionHistory(models.Model):
    family_member = models.ForeignKey(FamilyMember, on_delete=models.CASCADE)
    action =  models.ForeignKey(Earnable, on_delete=models.DO_NOTHING)
    FAIL = 0
    SUCCESS = 1
    PENDING = 2
    ERROR = 3
    STATUS = (
        (FAIL, _('Action failed unknown error, try again')),
        (SUCCESS, _('Action succeeded.')),
        (PENDING, _('Action pending.')),
        (ERROR, _('Error # FILL_ME_IN thrown'))
    )
    status = models.PositiveSmallIntegerField(choices=STATUS)
    action_date = models.DateTimeField('date action attempted')
    def get_status(self):
        return self.status
    def __str__(self):
        return self.action 